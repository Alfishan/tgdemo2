package com.xeme.tgdemo2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 4;

    TabFragMethods MtabFragMethods;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TabFragMethods) {

            MtabFragMethods = (TabFragMethods) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        super.onSaveInstanceState(savedInstanceState);

        View x = inflater.inflate(R.layout.tab_layout, null);

        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               // MtabFragMethods.hideit();
            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);


                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    if (i == 0)
                        tabLayout.getTabAt(i).setIcon(R.drawable.pvic1);
                    if (i == 1)
                        tabLayout.getTabAt(i).setIcon(R.drawable.pvic2);
                    if (i == 2)
                        tabLayout.getTabAt(i).setIcon(R.drawable.pvic3);
                    if (i == 3)
                        tabLayout.getTabAt(i).setIcon(R.drawable.pvic4);
                }
            }
        });

        return x;

    }


    public interface TabFragMethods {

        void hideit();

        void showit();

    }

    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Frag_P2P();
                case 1:
                    return new Frag_AT();
                case 2:
                    return new Frag_HR();
                case 3:
                    return new Frag_OT();

            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            /*switch (position) {
                case 0:
                    return "";//Point to Point
                case 1:
                    return "";//Airport Transfer
                case 2:
                    return "";//Hourly Rental
                case 3:
                    return "";//Outstation
            }*/
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {


        }

    }


}
