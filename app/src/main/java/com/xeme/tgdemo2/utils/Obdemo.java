package com.xeme.tgdemo2.utils;

import android.location.Location;
import android.util.Log;

import com.xeme.tgdemo2.Constants;

import java.util.Observable;

/**
 * Created by root on 18/5/16.
 */
public class Obdemo extends Observable {
static int i=0;
private Location mLocation;

    public Obdemo(Location mLng) {


       // MainActivity mainActivity=new MainActivity();
        //Frag_AT frag_at=new Frag_AT();

         //  this.addObserver(mainActivity);
       // this.addObserver(frag_at);

        i++;
        Log.d(Constants.TAG,"In Obdemo "+(i+1));
        mLocation=mLng;
    }

    public Location getlocation()
    {
        return mLocation;
    }

    public void setlocation(Location l)
    {
        i++;
        Log.d(Constants.TAG,"In setlocation "+(i+1));
        this.mLocation = l;
        this.setChanged();
        this.notifyObservers();
    }

}
