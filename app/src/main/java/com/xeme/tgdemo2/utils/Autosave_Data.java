package com.xeme.tgdemo2.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;

/**
 * Created by root on 12/4/16.
 */
public class Autosave_Data {


    public static final String KEY_PREF_NAME = "Auto_Save_Pref";
    private static final String TAG = "TGDemo2";

    //private final Context mCxt;
    private static Autosave_Data mInstance = null;
    Context mContext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences getpref;

    public Autosave_Data() {

//       mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);


    }/*(Context ctx) {
        this.mCxt = ctx;
    }*/

    public static Autosave_Data getInstance(Context ctx) {

        if (mInstance == null) {
            //   mInstance = new Autosave_Data(ctx);

        }
        return mInstance;
    }


    /* public Autosave_Data(Context context) {
         this.mContext = context;
     }
 */
    public void putdata(Context mContext, String KeyName, String value) {

        pref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        File f = new File("/data/data/" + mContext.getPackageName() + "/shared_prefs/" + KEY_PREF_NAME + ".xml");

        editor = pref.edit();
        editor.putString(KeyName, value);
        editor.apply();
    }

    public String getdata(Context mContext, String KeyName) {
        String text;
        getpref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        text = getpref.getString(KeyName, "NullData");
        return text;
    }

    public void cleardata(Context mContext) {

        SharedPreferences.Editor editor;
        getpref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        editor = getpref.edit();
        editor.clear();
        editor.commit();
    }
}