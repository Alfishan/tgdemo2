package com.xeme.tgdemo2;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xeme.tgdemo2.utils.Autosave_Data;

import org.w3c.dom.Document;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_P2P extends Fragment implements
        GeoQueryEventListener,
        LocationListener,
        GoogleMap.OnMapLoadedCallback,
        GoogleMap.OnCameraChangeListener,
        GoogleMap.OnMyLocationButtonClickListener,
        View.OnClickListener, PopupMenu.OnMenuItemClickListener {


    public View v;
    protected String mAddressOutput;
    protected boolean mAddressRequested;
    GoogleMap map;
    Context mContext;
    Button btn_confirm, btn_choose_car;
    FrameLayout fr_loc, fr_loc2;
    TextView tv_popup_pickup_address, tv_popup_drop_address;
    boolean onTop = true;
    Location mCurrentLocation;
    Location mCameraLocation;
    LatLng mCurrentLatLng;
    LatLng mCameraLatLng;
    Autosave_Data mAutosave_data = new Autosave_Data();
    MapView mapView;

    Location mLastLocation;
    GeoFire geoFire;
    GeoQuery geoQuery;
    Circle searchCircle;
    Map<String, Marker> markers;


    AppCompatImageView ivpin;

    LatLng center;
    TextView tv_location, tv_destination, tv_distance, tv_cost, tv_time;
    MarkerOptions markerOptions = new MarkerOptions();
    LatLng msource, mdestination;
    boolean viewclicktest;

    ViewGroup ll_btn;


    /**
     * Visible while the address is being fetched.
     */
    ProgressBar mProgressBar;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    Frag_P2pMethods mFrag_P2pMethods;

    public Frag_P2P() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Frag_P2pMethods) {

            mFrag_P2pMethods = (Frag_P2pMethods) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_p2, container, false);
        viewclicktest = true;
        mContext = getActivity().getBaseContext();

        Firebase.setAndroidContext(mContext);

        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        ivpin = (AppCompatImageView) v.findViewById(R.id.iv_pin);

        map = mapView.getMap();

        try {
            MapsInitializer.initialize(this.getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }


        // map.setPadding(0, 0, 0, 220);


        try {
            LatLng latLngCenter = new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude);
            searchCircle = map.addCircle(new CircleOptions().center(latLngCenter).radius(1000));
            //  searchCircle.setFillColor(Color.argb(66, 255, 0, 255));
            searchCircle.setStrokeColor(Color.argb(0, 0, 0, 0));

            // setup GeoFire
            this.geoFire = new GeoFire(new Firebase(Constants.GEO_FIRE_REF));
            // radius in km
            this.geoQuery = this.geoFire.queryAtLocation(Constants.INITIAL_CENTER, 1);

            // setup markers
            this.markers = new HashMap<String, Marker>();

            // map.setOnMarkerClickListener(this);


            mLastLocation = gpslocation.mLastLocation;
            // Needs to call MapsInitializer before doing any CameraUpdateFactory calls

        } catch (Exception e) {

            e.printStackTrace();
        }

        try {

            initview();

        } catch (SecurityException e) {

            e.printStackTrace();
        }
        // Updates the location and zoom of the MapView
        // CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
        // map.animateCamera(cameraUpdate);

        return v;
    }

    private void initview() {

        map.setOnMapLoadedCallback(this);
        fr_loc = (FrameLayout) v.findViewById(R.id.fr_loc);
        fr_loc2 = (FrameLayout) v.findViewById(R.id.fr_loc2);
        tv_popup_drop_address = (TextView) v.findViewById(R.id.tv_popup_drop_address);
        tv_popup_pickup_address = (TextView) v.findViewById(R.id.tv_popup_pickup_address);

        mCameraLocation = new Location(LocationManager.PASSIVE_PROVIDER);

        btn_confirm = (Button) v.findViewById(R.id.btn_confirm);
        btn_choose_car = (Button) v.findViewById(R.id.btn_choose_car);

        tv_distance = (TextView) v.findViewById(R.id.tv_estimated_distance);
        tv_cost = (TextView) v.findViewById(R.id.tv_estimated_cost);
        tv_time = (TextView) v.findViewById(R.id.tv_estimated_time);

        ll_btn = (ViewGroup) v.findViewById(R.id.ll_btn);


        btn_confirm.setOnClickListener(this);
        btn_choose_car.setOnClickListener(this);

        fr_loc.setOnClickListener(this);
        fr_loc2.setOnClickListener(this);



       /* btn_ridenow.setOnClickListener(this);
        btn_ridelater.setOnClickListener(this);*/
        //  tv_distance.setOnClickListener(this);
        //tv_cost.setOnClickListener(this);
        //tv_time.setOnClickListener(this);



        map.setOnCameraChangeListener(this);
        map.setOnMyLocationButtonClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        this.geoQuery.addGeoQueryEventListener(this);

    }

    @Override
    public void onStop() {
        super.onStop();

        this.geoQuery.removeAllListeners();
        for (Marker marker : this.markers.values()) {
            marker.remove();
        }


    }

    @Override
    public void onResume() {
        if (mapView != null) {
            mapView.onResume();
        }
        super.onResume();
        /*mapView.onResume();
        if (cp != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cp));
            cp = null;
        }*/


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }


    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        // Update the search criteria for this geoQuery and the circle on the map
        // radius in km


        mCameraLatLng = cameraPosition.target;
        mCameraLocation.setLatitude(mCameraLatLng.latitude);
        mCameraLocation.setLongitude(mCameraLatLng.longitude);


        Log.d("GeoFire", "Camera Zoom Level" + map.getCameraPosition().zoom);
        center = cameraPosition.target;
        if (map.getCameraPosition().zoom > 3) {

            try {

                //map.addMarker(new MarkerOptions().position(center).title("center"));
                double radius = zoomLevelToRadius(cameraPosition.zoom);
                this.searchCircle.setCenter(center);
                this.searchCircle.setRadius(radius);
                this.geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
                this.geoQuery.setRadius(radius / 1000);


            } catch (Exception e) {

                System.out.println("geoFire Error" + e.toString());
            }
        }

        if (onTop) {

            tv_popup_pickup_address.setText("Fetching Address...");
            gpslocation.getAddressFromLocation(mCameraLocation, mContext, new GeocoderHandler());
            msource = center;
            mAutosave_data.putdata(mContext, "tv_location", mAddressOutput);

        } else {
            tv_popup_drop_address.setText("Fetching Address...");
            gpslocation.getAddressFromLocation(mCameraLocation, mContext, new GeocoderHandler());
            mAutosave_data.putdata(mContext, "tv_destination", mAddressOutput);
            mdestination = center;
        }

        updateui();

    }

    @Override
    public void onMapLoaded() {
        try {
            startLocationUpdates();
            gpslocation.movetolastlocation(map);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);
            View locationButton = ((View) v.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            if (locationButton != null) {
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                rlp.setMargins(0, 0, 100, 250);
            }

        } catch (SecurityException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {
        // Add a new marker to the map
        Marker marker = this.map.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.longitude))
                .title(key.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        this.markers.put(key, marker);
    }

    @Override
    public void onKeyExited(String key) {
        // Remove any old marker
        Marker marker = this.markers.get(key);
        if (marker != null) {
            marker.remove();
            this.markers.remove(key);
        }
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        // Move the marker
        Marker marker = this.markers.get(key);
        if (marker != null) {
            this.animateMarkerTo(marker, location.latitude, location.longitude);
        }
    }

    @Override
    public void onGeoQueryReady() {
    }

    @Override
    public void onGeoQueryError(FirebaseError error) {
        new AlertDialog.Builder(mContext)
                .setTitle("Error")
                .setMessage("There was an unexpected error querying GeoFire: " + error.getMessage())
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void animateMarkerTo(final Marker marker, final double lat, final double lng) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long DURATION_MS = 3000;
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float elapsed = SystemClock.uptimeMillis() - start;
                float t = elapsed / DURATION_MS;
                float v = interpolator.getInterpolation(t);

                double currentLat = (lat - startPosition.latitude) * v + startPosition.latitude;
                double currentLng = (lng - startPosition.longitude) * v + startPosition.longitude;
                marker.setPosition(new LatLng(currentLat, currentLng));

                // if animation is not finished yet, repeat
                if (t < 1) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private boolean checkGooglePlayServices() {
        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(mContext);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
        /*
        * Google Play Services is missing or update is required
		*  return code could be
		* SUCCESS,
		* SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
		* SERVICE_DISABLED, SERVICE_INVALID.
		*/
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    getActivity(), Constants.REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;
    }

    private double zoomLevelToRadius(double zoomLevel) {
        // Approximation to fit circle into view
        return 16384000 / Math.pow(2, zoomLevel);
    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();

        switch (vid) {

            case R.id.btn_confirm:
                if (msource != null && mdestination != null)
                    new LongOperation().execute("");
                break;
            case R.id.btn_choose_car:
                try {


                    PopupMenu popup = new PopupMenu(getActivity(), btn_choose_car, Gravity.RELATIVE_LAYOUT_DIRECTION);
                    popup.getMenuInflater().inflate(R.menu.car_menu, popup.getMenu());
                    popup.setOnMenuItemClickListener(this);
                    popup.getMenu().add("Car7");

                    try {
                        Field mFieldPopup = popup.getClass().getDeclaredField("mPopup");
                        mFieldPopup.setAccessible(true);
                        MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popup);
                        mPopup.setForceShowIcon(true);
                    } catch (Exception e) {

                    }

                    popup.show();


                } catch (Exception e) {
                    System.out.println("Car Error:" + e);
                }
                break;

            case R.id.fr_loc:
                onTop = true;
                fr_loc.bringToFront();
                fr_loc.invalidate();
                fr_loc.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.bounce_anim));
                FindAddress(127);
                break;
            case R.id.fr_loc2:
                onTop = false;
                fr_loc2.bringToFront();
                fr_loc2.invalidate();
                fr_loc2.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.bounce_anim));
                FindAddress(128);
                break;

        }
    }

    protected void PickDropmarker(String Key, LatLng mLatLng, boolean oppcode) {

        Log.d("inPickDropmarker", "marker " + Key);
        Marker marker = this.markers.get(Key);
        Marker markernew;

        markerOptions.position(mLatLng);
        if (marker != null) {
            marker.remove();
            this.markers.remove(Key);

            if (oppcode)
                markerOptions.title("Pick up");
            else
                markerOptions.title("Destination");
            markernew = this.map.addMarker(markerOptions);
            this.markers.put(Key, markernew);
            Log.d("inPickDropmarker", "marker exist " + Key);

        } else {

            if (oppcode)
                markerOptions.title("Pick up");
            else
                markerOptions.title("Destination");//.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
            markernew = this.map.addMarker(markerOptions);
            this.markers.put(Key, markernew);
            Log.d("inPickDropmarker", "marker not exist " + Key);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        Log.d(Constants.TAG, "IN onActivityResult RCODE frag" + requestCode);
        switch (requestCode) {


            case 127:

                if (resultCode == Constants.RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(mContext, data);
                    Log.d(Constants.TAG, "Place: onresult frag" + place.getName());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), Constants.INITIAL_ZOOM_LEVEL));
                    if (onTop)
                        tv_popup_pickup_address.setText(place.getAddress());

                    msource = place.getLatLng();
                    //  map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));


                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(mContext, data);
                    // TODO: Handle the error.
                    Log.d(Constants.TAG, status.getStatusMessage());

                } else if (resultCode == Constants.RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            case 128:

                if (resultCode == Constants.RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(mContext, data);
                    Log.d(Constants.TAG, "Place: onresult frag" + place.getName());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), Constants.INITIAL_ZOOM_LEVEL));
                    if (!onTop)
                        tv_popup_drop_address.setText(place.getAddress());
                    mdestination = place.getLatLng();

                    //  map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));


                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(mContext, data);
                    // TODO: Handle the error.
                    Log.d(Constants.TAG, status.getStatusMessage());

                } else if (resultCode == Constants.RESULT_CANCELED) {
                    // The user canceled the operation.
                }
        }


    }

    void updateui() {
        if (msource != null && mdestination != null) {

            if (ll_btn.getVisibility() != View.VISIBLE) {
                ll_btn.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.bottom_slide_up));
                ll_btn.setVisibility(View.VISIBLE);

               /* TranslateAnimation animate = new TranslateAnimation(0,0,ll_btn.getHeight(),0);
                animate.setDuration(500);
                animate.setFillAfter(true);
                ll_btn.startAnimation(animate);
                ll_btn.setVisibility(View.VISIBLE);*/
            }


        }
    }

    void FindAddress(int rcode) {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(getActivity());
            getParentFragment().startActivityForResult(intent, rcode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(mContext, "Id:" + item, Toast.LENGTH_SHORT).show();
        return false;
    }

    private void startLocationUpdates() {
        try {

            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        gpslocation.mGoogleApiClient, gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }

    }


    public interface Frag_P2pMethods {

        void placecameraat(LatLng mLatLng);

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String result;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    result = bundle.getString("address");
                    break;
                default:
                    result = null;
            }
            if (onTop)
                tv_popup_pickup_address.setText(result);
            else
                tv_popup_drop_address.setText(result);

        }
    }

    private class LongOperation extends AsyncTask<String, Void, PolylineOptions> {

        private PolylineOptions getDirection() {
            try {

                GMapV2Direction md = new GMapV2Direction();

                Document doc = md.getDocument(msource, mdestination,
                        GMapV2Direction.MODE_DRIVING);

                final String Distance = md.getDistanceText(doc);
                final String time = md.getDurationText2(doc);
                final float getDistanceValue = ((md.getDistanceValue(doc)) / 1000) * 15;

                Log.d("Distance : ", Distance + " " + time + " distance value" + getDistanceValue);

                try {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            tv_distance.setText(Distance);
                            tv_time.setText(time);
                            // Log.d("Distance :", "  Duration:" + md.getDurationText2(doc)+String.valueOf(cost));
                            tv_cost.setText(String.valueOf(getDistanceValue));

                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }


                ArrayList<LatLng> directionPoint = md.getDirection(doc);
                PolylineOptions rectLine = new PolylineOptions().width(12).color(
                        Color.parseColor("#2196F3")).geodesic(true);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                // isDirectionDrawn = true;

                return rectLine;
            } catch (Exception e) {
                ///possible error:
                ///java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): Map size can't be 0. Most likely, layout has not yet occured for the map view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map's dimensions.
                return null;
            }

        }

        @Override
        protected PolylineOptions doInBackground(String... params) {
            PolylineOptions polylineOptions = null;
            try {
                polylineOptions = getDirection();
            } catch (Exception e) {
                Thread.interrupted();
            }
            return polylineOptions;
        }

        @Override
        protected void onPostExecute(PolylineOptions result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you

            map.clear();///TODO: clean the path only..

            map.addMarker(new MarkerOptions()
                    .position(mdestination)
                    .title("Your Selected Drop Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            map.addMarker(new MarkerOptions()
                    .position(msource)
                    .title("Your Selected Pick Up Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            Marker s = map.addMarker(new MarkerOptions()
                    .position(result.getPoints().get(0))
                    .title("Nearest Pick Up Location")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

            Marker d = map.addMarker(new MarkerOptions()
                    .position(result.getPoints()
                            .get(result.getPoints().size() - 1))
                    .title("Nearest Drop Location")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

            LatLngBounds.Builder bc = new LatLngBounds.Builder();
            bc.include(s.getPosition());
            bc.include(d.getPosition());
            //   map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));

            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 100));
            map.addPolyline(result);
            // zoomToPoints();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}


