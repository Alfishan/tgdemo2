package com.xeme.tgdemo2;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_AT extends Fragment implements
        GoogleMap.OnMapLoadedCallback,
        View.OnClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        LocationListener,
        GoogleMap.OnCameraChangeListener {


    public View v;

    MapView mapView;
    Context mContext;
    GoogleMap map;
    Button btn_select_car;
    FrameLayout fr_loc, fr_loc2;
    TextView tv_popup_pickup_address, tv_popup_drop_address;
    boolean onTop = true;
    Location mCurrentLocation;
    Location mCameraLocation;
    LatLng mCurrentLatLng;
    LatLng mCameraLatLng;

    public Frag_AT() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_p2, container, false);
        mContext = getActivity();
        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        map = mapView.getMap();

        map.setOnMapLoadedCallback(this);
        map.setOnMyLocationButtonClickListener(this);
        try {
            MapsInitializer.initialize(this.getActivity());

        } catch (Exception e) {
            e.printStackTrace();
        }
        initview();

        return v;
    }

    private void initview() {
        btn_select_car = (Button) v.findViewById(R.id.btn_choose_car);
        mCameraLocation=new Location(LocationManager.PASSIVE_PROVIDER);
        tv_popup_drop_address = (TextView) v.findViewById(R.id.tv_popup_drop_address);
        tv_popup_pickup_address = (TextView) v.findViewById(R.id.tv_popup_pickup_address);

        fr_loc = (FrameLayout) v.findViewById(R.id.fr_loc);
        fr_loc2 = (FrameLayout) v.findViewById(R.id.fr_loc2);
        fr_loc.setOnClickListener(this);
        fr_loc2.setOnClickListener(this);
        btn_select_car.setOnClickListener(this);


        View locationButton = ((View) v.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        if (locationButton != null) {
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 100, 250);
        }
        map.setOnCameraChangeListener(this);

    }


    @Override
    public void onResume() {
        if (mapView != null) {
            mapView.onResume();
        }
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    public void onMapLoaded() {

        try {
            startLocationUpdates();
            gpslocation.movetolastlocation(map);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.setMyLocationEnabled(true);
        } catch (SecurityException e) {

            e.printStackTrace();
        }

        //  mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));

    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();

        switch (vid) {

            case R.id.btn_choose_car:

                break;
            case R.id.fr_loc:
                onTop = true;
                fr_loc.bringToFront();
                fr_loc.invalidate();
                fr_loc.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.bounce_anim));
                FindAddress(130);
                break;
            case R.id.fr_loc2:
                onTop = false;
                fr_loc2.bringToFront();
                fr_loc2.invalidate();
                fr_loc2.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.bounce_anim));
                FindAddress(131);
                break;


        }

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }


    private void startLocationUpdates() {

        try {
            gpslocation.mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gpslocation.mGoogleApiClient);

            if (gpslocation.mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        gpslocation.mGoogleApiClient, gpslocation.mLocationRequest, this);
            }
        } catch (SecurityException e) {

            e.printStackTrace();
        }


    }

    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = location;
        mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        Log.d(Constants.TAG, "on f_AT loc update");


    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        mCameraLatLng = cameraPosition.target;
        mCameraLocation.setLatitude(mCameraLatLng.latitude);
        mCameraLocation.setLongitude(mCameraLatLng.longitude);


        if (onTop) {

            tv_popup_pickup_address.setText("Fetching Address...");
            gpslocation.getAddressFromLocation(mCameraLocation, mContext, new GeocoderHandler());

        } else {
            tv_popup_drop_address.setText("Fetching Address...");
            gpslocation.getAddressFromLocation(mCameraLocation, mContext, new GeocoderHandler());
        }

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String result;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    result = bundle.getString("address");
                    break;
                default:
                    result = null;
            }
           if(onTop)
            tv_popup_pickup_address.setText(result);
            else
               tv_popup_drop_address.setText(result);

        }
    }

    void FindAddress(int rcode) {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(getActivity());
            getParentFragment().startActivityForResult(intent, rcode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        Log.d(Constants.TAG, "IN onActivityResult RCODE frag" + requestCode);
        switch (requestCode) {

            case 130:

                if (resultCode == Constants.RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(mContext, data);
                    Log.d(Constants.TAG, "Place: onresult frag" + place.getName());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), Constants.INITIAL_ZOOM_LEVEL));
                    if (onTop)
                        tv_popup_pickup_address.setText(place.getAddress());


                    //  map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));


                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(mContext, data);
                    // TODO: Handle the error.
                    Log.d(Constants.TAG, status.getStatusMessage());

                } else if (resultCode == Constants.RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            case 131:

                if (resultCode == Constants.RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(mContext, data);
                    Log.d(Constants.TAG, "Place: onresult frag" + place.getName());
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), Constants.INITIAL_ZOOM_LEVEL));
                    if (!onTop)
                        tv_popup_drop_address.setText(place.getAddress());
                   // mdestination = place.getLatLng();

                    //  map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));

                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(mContext, data);
                    // TODO: Handle the error.
                    Log.d(Constants.TAG, status.getStatusMessage());

                } else if (resultCode == Constants.RESULT_CANCELED) {
                    // The user canceled the operation.
                }
        }


    }
}
