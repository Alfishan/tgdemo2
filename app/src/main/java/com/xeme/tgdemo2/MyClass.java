package com.xeme.tgdemo2;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MyClass {
    private List<MyListener> listeners = new ArrayList<MyListener>();

    void addListener(MyListener listener) {

        Log.d(Constants.TAG, "MyListener Added " + listener.toString());
    }



   void notifySomethingHappened() {
        for ( MyListener listener : listeners) {

            try {
                listener.somethingHappened();
                listener.notify();
                Log.d(Constants.TAG, "MyListener notify " + listener.toString());
            }
            catch (Exception e){


            }




        }
    }


}
