package com.xeme.tgdemo2;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.xeme.tgdemo2.utils.Obdemo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import interfaces.MYCallbacks;

/**
 * Created by root on 18/5/16.
 */
public class gpslocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, MYCallbacks.MyLocationChanged, MyListener, Observer {


    public static GoogleApiClient mGoogleApiClient;
    public static LocationRequest mLocationRequest;
    public static Location mLastLocation;
    public static Location mCurrentLocation;
    public static boolean isconnected = false;
    Context mContext;
    MYCallbacks myCallbacks;
    MyClass myClass;
    Obdemo obdemo;


    public gpslocation(Context m) {


        mContext = m;
        myCallbacks = new MYCallbacks();
        myCallbacks.register(this);
        myClass = new MyClass();
        myClass.addListener(this);
        try {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            createLocationRequest();
        } catch (Exception e) {
            Log.d(Constants.TAG, e.toString());
        }
        obdemo = new Obdemo(mLastLocation);
    }

    public static LatLng getlatlong() {
        if (mCurrentLocation != null) {

            return new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
        return new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude);
    }

    public static void movetolastlocation(GoogleMap map) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(getlatlong(), Constants.INITIAL_ZOOM_LEVEL));
    }

    public static void getAddressFromLocation(
            final Location location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                String result2="";
                try {
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0) {

                        Address address = addresses.get(0);
                        ArrayList<String> addressFragments = new ArrayList<String>();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                if (!address.getAddressLine(i).equals("null")) {
                                    addressFragments.add(address.getAddressLine(i));
                                    Log.d(Constants.TAG,"line "+i+" "+address.getAddressLine(i));
                                }
                            Log.wtf(Constants.TAG, "Address Detail" + address.getAddressLine(i));
                        }
                        // sending back first address line and locality

                        for (String obj:addressFragments ){

                            if (!obj.equals("null"))

                                result2 = result2 +( obj +", ");
                        }

                        result=result2;
                       // result; /* = TextUtils.join(System.getProperty("line.separator"), addressFragments);*/
                    }
                } catch (IOException e) {
                    Log.e(Constants.TAG, "Impossible to connect to Geocoder", e);

                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isconnected = true;

        startLocationUpdates();

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            // Determine whether a Geocoder is available.
            if (!Geocoder.isPresent()) {

                return;
            }


        }

    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        isconnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onLocationChanged(Location location) {

        //obdemo.setlocation(location);
        // myClass.notifySomethingHappened();
        mCurrentLocation = location;
        //   MmyLocation.MyLocationChanged(new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude));
        //myCallbacks.setMyLocation(new LatLng(Constants.INITIAL_CENTER.latitude, Constants.INITIAL_CENTER.longitude));

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void MyLocationChangedcallbackReturn(LatLng latLng) {
        Toast.makeText(mContext, "latlong updated", Toast.LENGTH_SHORT).show();
    }


/*    public interface MyLocation {
        void MyLocationChanged(LatLng latLng);
    }

    public void register(MyLocation LocationChanged) {
        MmyLocation = LocationChanged;

    }

    public void setMyLocation(LatLng latLng){
        MmyLocation.MyLocationChanged(latLng);


    }*/

    @Override
    public void somethingHappened() {
        Toast.makeText(mContext, "somethingHappened gps", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void update(Observable observable, Object data) {

        Toast.makeText(mContext, "update", Toast.LENGTH_SHORT).show();
    }

}
