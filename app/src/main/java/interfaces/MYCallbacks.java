package interfaces;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 12/5/16.
 */
public class MYCallbacks {
    MyCallbackClass myCallbackClass;
    MyLocationChanged myLocationChanged;
    LatLng mCurrent;
    private List<MyLocationChanged> listeners = new ArrayList<MyLocationChanged>();

    public void registerCallback(MyCallbackClass callbackClass) {
        myCallbackClass = callbackClass;

    }

    public interface MyCallbackClass {
        void callbackReturn();
    }

    void doSomething() {

        myCallbackClass.callbackReturn();
    }


    public void register(MyLocationChanged LocationChanged) {
        listeners.add(LocationChanged);

    }


    public interface MyLocationChanged {
        void MyLocationChangedcallbackReturn(LatLng latLng);
    }

   public void setMyLocation(LatLng latLng){
       for(MyLocationChanged listener : listeners){
           listener.MyLocationChangedcallbackReturn(latLng);
       }

    }



  /*public  void checkperm(Context ctx) {

        final Context ctxinner = ctx;
        Globctx = ctx;
//      final Activity activity = (Activity) ctx;


        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            /*//**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            /*//**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(this., 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }
*/



}
